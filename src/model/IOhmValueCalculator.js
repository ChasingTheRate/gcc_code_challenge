import bandValues from './resistorColors.json';

class IOhmValueCalculator {
  
  static get bandValues() {
    return bandValues;
  }

  static CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor) {
    let bandAValue = bandValues.bandAB[bandAColor].value;
    let bandBValue = bandValues.bandAB[bandBColor].value;
    let bandCValue = bandValues.bandC[bandCColor].value;

    return (Number(`${bandAValue}${bandBValue}`) * bandCValue);
  }

  static CalculateOhmValueAsString(bandAColor, bandBColor, bandCColor, bandDColor) {
    let ohmValue = IOhmValueCalculator.CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor)
    let unit = bandValues.bandC[bandCColor].unit;
    let tolerance = bandValues.bandD[bandDColor].displayValue;

    return `${ ohmValue } ${ unit } ${ tolerance }`
  }
}

export default IOhmValueCalculator;
