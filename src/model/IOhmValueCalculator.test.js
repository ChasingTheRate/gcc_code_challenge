var assert = require('assert');
import IOhmValueCalculator from './IOhmValueCalculator';

describe('IOhmValueCalculator', function() {
  describe('Colors: brown, black, orange, gold', () => {
    const value = IOhmValueCalculator.CalculateOhmValueAsString('brown', 'black', 'orange', 'gold');
    it('should return 10 KΩ ± 5%', () => {
      assert.equal('10 KΩ ± 5%', value);
    });
  });
  describe('Colors: brown, black, brown, gold', () => {
    const value = IOhmValueCalculator.CalculateOhmValueAsString('brown', 'black', 'brown', 'gold');
    it('should return 100 Ω ± 5%', () => {
      assert.equal('100 Ω ± 5%', value);
    });
  });
  describe('Colors: brown, black, red, silver', () => {
    const value = IOhmValueCalculator.CalculateOhmValueAsString('brown', 'black', 'red', 'silver');
    it('should return 1000 Ω ± 10%', () => {
      assert.equal('1000 Ω ± 10%', value);
    });
  });
});