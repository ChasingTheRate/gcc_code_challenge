import React, { Component } from 'react';
import './App.scss';
import MainPage from './pages/MainPage/MainPage';
import NavigationBar from './pages/MainPage/components/NavigationBar/NavigationBar';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavigationBar/>
        <MainPage/>
      </div>
    );
  }
}

export default App;