import React from 'react';
import './MainPage.scss';
import Resistor from './components/Resistor/Resistor';
import IOhmValueCalculator from '../../model/IOhmValueCalculator';

import ColorSelectionModal from '../../modals/colorSelectionModal/colorSelectionModal';

class MainPage extends React.Component {
  constructor(props) {
    super(props)

    this.colorList = IOhmValueCalculator.bandValues.bandC,
    this.activeBand = '';
    this.bandData = IOhmValueCalculator.bandValues;
    
    this.getBandColors = this.getBandColors.bind(this);
    this.didSelectBand = this.didSelectBand.bind(this);
    this.didSelectBandColor = this.didSelectBandColor.bind(this);
    this.showModalWithBandId = this.showModalWithBandId.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      bands: {
        a: {
          color: 'brown',
          displayValue: '5'
        },
        b: {
          color: 'black',
          displayValue: '6'
        },
        c: {
          color: 'orange',
          displayValue: '1KΩ'
        },
        d: {
          color: 'gold',
          displayValue: "± 5%"
        }
      },
      showModal: false,
      resistanceValue: "1 KΩ ± 5%"
    }
  }

  getBandColors() {
    return [
      this.state.bands.a.color,
      this.state.bands.b.color,
      this.state.bands.c.color,
      this.state.bands.d.color,
    ];
  }

  didSelectBand(bandId) {
    this.showModalWithBandId(bandId);
  }

  didSelectBandColor(color) {
    // console.log(`band: ${ this.activeBand }, color: ${ color }`);
    const bands = this.state.bands;
    bands[this.activeBand].color = color;
    this.setState({
      showModal: false,
      bands
    });
    const newResistanceValue = IOhmValueCalculator.CalculateOhmValueAsString(bands.a.color,bands.b.color,bands.c.color,bands.d.color)
    this.setState({
      resistanceValue: newResistanceValue
    });
  }

  showModalWithBandId(bandId) {
    this.activeBand = bandId
    switch (bandId) {
      case 'a':
      case 'b':
        this.colorList = this.bandData.bandAB;
        break;
      case 'c':
        this.colorList = this.bandData.bandC;
        break;
      default:
      this.colorList = this.bandData.bandD;
        break;
    }
    this.setState({
      showModal: true
    });
  }

  closeModal() {
    this.setState({
      showModal: false
    });
  }

  render() {
    return (
      <div className="main-container">
        <div className="resistor-wrapper">
          <Resistor bandColors={ this.getBandColors()} didSelectBand={ this.didSelectBand }></Resistor>
        </div>
        <div className="resistance-value-wrapper">
          <div className="resistance-value-text">{ this.state.resistanceValue }</div>
        </div>
        { this.state.showModal &&
        <ColorSelectionModal 
          colorList={ this.colorList }
          modalTitle="Select Color"
          didSelectColorBand= {this.didSelectBandColor }
          closeModal={  this.closeModal }
          ></ColorSelectionModal> }
      </div>
    )
  }
  
}

export default MainPage;

