import React from 'react';
import './navigationBar.scss';

class NavigationBar extends React.Component {
  render() {
    return (
      <div className="navigation-bar-container">
        <div className="nav-bar-text-container">
          <div id="nav-title">Resistance Calc</div>
        </div>    
      </div>
    );
  }
}

export default NavigationBar;