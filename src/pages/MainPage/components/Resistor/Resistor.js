import React from 'react';
import './Resistor.scss';


var Resistor = (props) => {

  const bandAStyle = {
    backgroundColor: props.bandColors[0]
  }

  const bandBStyle = {
    backgroundColor: props.bandColors[1]
  }

  const bandCStyle = {
    backgroundColor: props.bandColors[2]
  }

  const bandDStyle = {
    backgroundColor: props.bandColors[3]
  }

  const didClickButtonBand = (bandId) => {
    // console.log(bandId);
    props.didSelectBand(bandId);
  }

  return (
    <div className="resistor-container">
      <div className="resistor-body">
        <input type="button" id="band-a" className="band" style={ bandAStyle } onClick={ () => { didClickButtonBand('a') } }/>
        <input type="button" id="band-b" className="band" style={ bandBStyle } onClick={ () => { didClickButtonBand('b') } }/>
        <input type="button" id="band-c" className="band" style={ bandCStyle } onClick={ () => { didClickButtonBand('c') } }/>
        <input type="button" id="band-d" className="band" style={ bandDStyle } onClick={ () => { didClickButtonBand('d') } }/>
      </div>
    </div>
  )
}

export default Resistor;
