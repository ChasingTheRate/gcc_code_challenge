import React from 'react';
import images from '../../../public/assets/images';
import './colorSelectionModal.scss';

class ColorSelectionModal extends React.Component{
  constructor(props) {
    super(props);

    this.colorListItems = this.colorListItems.bind(this);
    this.didSelectColorItem = this.didSelectColorItem.bind(this);
    this.closeModal = this.closeModal.bind(this);

    // console.log(JSON.stringify(props.colorList, null, 1));
  }

  didSelectColorItem(color) {
    this.props.didSelectColorBand(color);
  }

  colorListItems() {
      return Object.keys(this.props.colorList).map((key) => {
        const style = {
          backgroundColor: key
        }

        return (
          <input 
            type="button" 
            className="color-item" 
            key={key} 
            style={style}
            onClick={ () => this.didSelectColorItem(key) }
            value= { this.props.colorList[key].displayValue }
          />
        )
      })
  }

  closeModal(event) {
    event.preventDefault();
    this.props.closeModal();
  }

  render(){
      this.colorListItems();
    return(
      <div className="mask">
        <div className="modal-container">
          <div className="modal-header">
            <div className="modal-header-text">{ this.props.modalTitle }</div>
            <div className="modal-header-close">
              <input type="image" src={ images.close } onClick={ this.closeModal }></input>
            </div>
          </div>
          <div className="modal-body">
            <div className="color-items-container">
              { this.colorListItems() }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ColorSelectionModal;